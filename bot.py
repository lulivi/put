# Copyright (c) 2022 Luis Liñán
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Track the daily push-ups!"""

import datetime
import logging
import os
import random

from enum import Enum, auto
from typing import Callable, Dict, Final, List, Optional, Union

from dotenv import load_dotenv
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.constants import ParseMode
from telegram.error import BadRequest
from telegram.ext import (
    Application,
    CallbackContext,
    CallbackQueryHandler,
    CommandHandler,
    ContextTypes,
    PicklePersistence,
)

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)


class PushUpTrackerBaseException(Exception):
    """Base exception for the call-up tracker errors."""


class PushUpperAlreadyEnrolledError(PushUpTrackerBaseException):
    """When the new push-upper is already enrolled in the tracker."""

    def __init__(self, push_upper: "PushUpper") -> None:
        self.push_upper = push_upper
        self.message = f"The push-upper `{self.push_upper}` is already enrolled"
        super().__init__(self.message)


class PushUpperNotEnrolledError(PushUpTrackerBaseException):
    """When the push-upper is not enrolled in the tracker."""

    def __init__(self, push_upper: "PushUpper") -> None:
        self.push_upper = push_upper
        self.message = f"The push-upper `{self.push_upper}` is not enrolled"
        super().__init__(self.message)


class PushUpperAlreadyDoneError(PushUpTrackerBaseException):
    """The push-upper is already done."""

    def __init__(self, push_upper: "PushUpper") -> None:
        self.push_upper = push_upper
        self.message = f"The push-upper `{self.push_upper}` is already done"
        super().__init__(self.message)


class PushUpperAlreadyPunishedError(PushUpTrackerBaseException):
    """The push-upper is already done."""

    def __init__(self, push_upper: "PushUpper") -> None:
        self.push_upper = push_upper
        self.message = f"The push-upper `{self.push_upper}` is has already a punish"
        super().__init__(self.message)


class NoTrackerFoundError(PushUpTrackerBaseException):
    """No tracker has been found."""


class InvalidTrackerButtonError(PushUpTrackerBaseException):
    """The message id is different than the one expected."""

    def __init__(self, message: str = "") -> None:
        self.message = message or "The pushed message was not the last one."
        super().__init__(self.message)


def get_tracker(chat_data: Optional[dict]) -> "PushUpTracker":
    """Obtain the tracker if existent."""
    if chat_data is None:
        raise NoTrackerFoundError("Could not find a tracker instance. Please, create a new one.")

    tracker = chat_data.get("push-up-tracker", None)

    if tracker is None:
        raise NoTrackerFoundError("Could not find a tracker instance. Please, create a new one.")

    return tracker


async def bot_call_function_with_keyboard(bot_function: Callable, *args, **kwargs) -> None:
    """Call a bot function with the markup keyboard."""
    await bot_call_function(
        bot_function=bot_function,
        reply_markup=PUSH_UPS_KEYBOARD,
        *args,
        **kwargs,
    )


async def bot_call_function(bot_function: Callable, *args, **kwargs) -> None:
    """Call a bot function."""
    try:
        await bot_function(parse_mode=ParseMode.MARKDOWN, *args, **kwargs)
    except BadRequest as error:
        if "Message is not modified" in str(error):
            return
        raise


class Button(Enum):
    PUSHUPS_DONE = auto()
    PUNISHMENT_DONE = auto()


class DoneStatus(Enum):
    DONE = "🔴"
    UNDONE = "❎"

    def __str__(self) -> str:
        return self.value


class PunishStatus(Enum):
    PUNISH = "☠️"
    UNPUNISH = "😇"

    def __str__(self) -> str:
        return self.value


class PushUpper:
    def __init__(self, id: int, name: str) -> None:
        self.__id: int = id
        self.__name: str = name
        self.__done = DoneStatus.UNDONE
        self.__punished = PunishStatus.UNPUNISH

    @property
    def id(self) -> int:
        return self.__id

    @property
    def name(self) -> int:
        return self.__name

    @property
    def done(self) -> str:
        return self.__done

    @done.setter
    def done(self, value: DoneStatus) -> None:
        self.__done = value

    @property
    def punished(self) -> str:
        return self.__punished

    @punished.setter
    def punished(self, value: bool) -> None:
        self.__punished = value

    def reset_push_ups(self) -> None:
        self.__done = DoneStatus.UNDONE

    def __eq__(self, other: "PushUpper") -> bool:
        return other.id == self.id

    def __str__(self) -> str:
        return self.name


class PushUpTracker:
    __slots__ = ["__push_uppers", "__chat_id", "__message_id", "__actions"]

    def __init__(self, chat_id: int, message_id: int):
        self.__push_uppers: List[PushUpper] = []
        self.__chat_id: int = chat_id
        self.__message_id: int = message_id
        self.__actions: Dict[str, Callable] = {
            Button.PUSHUPS_DONE.name: self.push_ups_done,
            Button.PUNISHMENT_DONE.name: self.punishment_done,
        }

    @property
    def chat_id(self) -> int:
        return self.__chat_id

    def ids_as_dict(self) -> Dict[str, int]:
        """Return the chat and message ids.

        :returns: Dictionary containing the chat and message ids.

        """
        return {"chat_id": self.__chat_id, "message_id": self.__message_id}

    def bot_repr(self) -> Dict[str, Union[int, str]]:
        """Return all the bot necessary arguments to update the tracker message.

        :returns: Dictionary containing the text, the chat and message ids.

        """
        return {"text": str(self), **self.ids_as_dict()}

    def validate_message_id(self, message_id: int) -> None:
        """Check whether the button message_id is the same as this instance message id.

        :param message_id: The input message id.
        :raises InvalidTrackerButtonError: If the message id is different.

        """
        if self.__message_id != message_id:
            raise InvalidTrackerButtonError()

    def enroll(self, new_push_upper: PushUpper) -> None:
        """Enroll a player into the tracker.

        :param new_push_upper: The new player.
        :raises PushUpperAlreadyEnrolledError: If the player is already enrolled.

        """
        if new_push_upper in self.__push_uppers:
            raise PushUpperAlreadyEnrolledError(new_push_upper)
        self.__push_uppers.append(new_push_upper)

    def leave(self, push_upper: PushUpper) -> None:
        """Remove a player from the tracker.

        :param push_upper: The player to remove.
        :raises PushUpperNotEnrolledError: If the player was not enrolled.

        """
        try:
            self.__push_uppers.remove(push_upper)
        except ValueError as error:
            raise PushUpperNotEnrolledError(push_upper) from error

    def reset_push_ups(self) -> None:
        """Reset the players push-ups."""
        for push_upper in self.__push_uppers:
            push_upper.reset_push_ups()

    def do(self, action: str, push_upper: PushUpper) -> str:
        """Perform an action.

        :param action: Or the push ups are done, or the punishment is fulfilled.
        :param push_upper: The player that completed the task.
        :returns: A message result of the act.

        """
        return self.__actions[action](push_upper)

    def push_ups_done(self, push_upper: PushUpper) -> str:
        """Check that a player has completed the push-ups.

        :param push_upper: The player that finished the assignment.
        :raises PushUpperAlreadyDoneError: If the player has already done their work.
        :returns: A message result of the act.

        """
        found_push_upper = self.__find_push_upper(push_upper)

        if found_push_upper.done is DoneStatus.DONE:
            raise PushUpperAlreadyDoneError(push_upper)

        found_push_upper.done = DoneStatus.DONE
        return f"`{push_upper}` did the push-ups for today!"

    def punishment_done(self, push_upper: PushUpper) -> str:
        """Check that the player completed the punishment.

        :param push_upper: The player that has to be punished.
        :raises PushUpperAlreadyPunishedError: If the player has already a punishment to do.
        :returns: A message result of the act.

        """
        found_push_upper = self.__find_push_upper(push_upper)

        if found_push_upper.punished is PunishStatus.UNPUNISH:
            raise PushUpperAlreadyPunishedError(push_upper)

        found_push_upper.punished = PunishStatus.UNPUNISH
        return f"`{push_upper}` finished the punishment for today!"

    def whos_guilty(self) -> List[PushUpper]:
        """Get the list of players that should be punished.

        :returns: The list of player names.

        """
        return [pu for pu in self.__push_uppers if pu.done is DoneStatus.UNDONE]

    def punish_perpetrators(self) -> List[PushUpper]:
        """Get and punish the list of players that should be punished.

        :returns: The list of player names.

        """
        perpetrators = self.whos_guilty()

        for push_upper in perpetrators:
            push_upper.punished = PunishStatus.PUNISH

        return perpetrators

    def __find_push_upper(self, push_upper) -> PushUpper:
        """Find the push-upper in the list of enrolled players.

        :param push_upper: The player that finished the assignment.
        :raises PushUpperNotEnrolledError: If the player is not enrolled in the tracker.
        :returns: The found player.

        """
        try:
            return self.__push_uppers[self.__push_uppers.index(push_upper)]
        except ValueError as error:
            raise PushUpperNotEnrolledError(push_upper) from error

    def __str__(self) -> str:
        player_list_str = "".join(
            [
                f"\n· {str(push_upper.punished)} {str(push_upper.done)} `{push_upper.name}`"
                for push_upper in self.__push_uppers
            ]
        )
        leyend = (
            f"Done status:\n· Done: {str(DoneStatus.DONE)}\n· Not yet done:"
            f" {str(DoneStatus.UNDONE)}\n\nPunish status:\n· Should be punished:"
            f" {str(PunishStatus.PUNISH)}\n· No punishment: {str(PunishStatus.UNPUNISH)}"
        )
        return f"Push-uppers list:\n{player_list_str}\n\n{leyend}"


PUSH_UPS_KEYBOARD: Final[InlineKeyboardMarkup] = InlineKeyboardMarkup(
    [
        [InlineKeyboardButton("Flexiones hechas!!!", callback_data=Button.PUSHUPS_DONE.name)],
        [InlineKeyboardButton("Castigo hecho!!!", callback_data=Button.PUNISHMENT_DONE.name)],
    ]
)

PUNISHMENTS = [
    "ordenar cuarto/cocina",
    "subir y bajar escaleras 5 minutos",
    "tirar la basura más lejos de lo normal",
    "ir en chanclas a comprar/sacar al perro, cosas breves",
    "Barrer y fregar un cuarto que ya esté limpio",
    "sacar los cordones de todas las zapatillas y volverlos a poner",
    "rellenar una hoja copiando 'Debo hacer diez flexiones cada día'",
    "vestirse y desvestirse 5 veces completamente",
    "tomarte un vaso de agua a cucharadas pequeñas",
    "grabar un audio leyendo un artículo de wikipedia durante 5 minutos (y repitiéndolo si se"
    " acaba)",
    "mantener una posición incómoda durante un tiempo",
    "doblar y desdoblar todos los calcetines",
    "castigado sin usar los pies durante X minutos",
    "mirar una pared",
    "escuchar en bucle una canción tipo la de Nyan cat",
    "memorizar una lista o un texto y recitarlo. Si te equivocas se repite el castigo ",
    "doblar y desdoblar ropa random",
]


async def command_help(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Show help.

    :param update: The event that caused the bot petition.
    :param context: The information related to the update.

    """
    await update.message.reply_text(
        "Use the command `/start` to create a new tracker.\n\nSend `/join` / `/enroll` to join the"
        " tracker.\n\nSend `/leave` to remove yourself from the tracker.\n\nSend `/reset` to clean"
        " the players records"
    )


async def command_start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Start a new Push-up tracker.

    :param update: The event that caused the bot petition.
    :param context: The information related to the update.

    """
    message_id = (
        await context.bot.send_message(
            chat_id=update.message.chat_id,
            text="Push-uppers list:",
            reply_markup=PUSH_UPS_KEYBOARD,
        )
    ).id
    context.chat_data["push-up-tracker"] = PushUpTracker(
        chat_id=update.message.chat_id, message_id=message_id
    )
    for job in context.job_queue.jobs():
        job.schedule_removal()

    context.job_queue.run_daily(
        callback=distribute_punishments,
        time=datetime.time(hour=5, tzinfo=datetime.datetime.now().astimezone().tzinfo),
        chat_id=update.message.chat_id,
        data={"chat_id": update.message.chat_id},
    )
    context.job_queue.run_daily(
        callback=remind_push_ups,
        time=datetime.time(hour=9, tzinfo=datetime.datetime.now().astimezone().tzinfo),
        chat_id=update.message.chat_id,
        data={
            "chat_id": update.message.chat_id,
            "message": "this seems like a reasonable time to do push-ups!",
        },
    )
    context.job_queue.run_daily(
        callback=remind_push_ups,
        time=datetime.time(hour=22, tzinfo=datetime.datetime.now().astimezone().tzinfo),
        chat_id=update.message.chat_id,
        data={
            "chat_id": update.message.chat_id,
            "message": "do the push-ups before the day finishes!",
        },
    )


async def command_enroll(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Enroll in the game.

    :param update: The event that caused the bot petition.
    :param context: The information related to the update.

    """
    try:
        tracker = get_tracker(context.chat_data)
    except NoTrackerFoundError as error:
        await bot_call_function(bot_function=update.message.reply_text, text=str(error))
        return

    new_player = PushUpper(update.message.from_user.id, update.message.from_user.full_name)
    try:
        tracker.enroll(new_player)
    except PushUpperAlreadyEnrolledError as error:
        await bot_call_function(bot_function=update.message.reply_text, text=str(error))
        return

    await update.message.delete()
    await bot_call_function(
        bot_function=context.bot.send_message,
        chat_id=tracker.chat_id,
        text=f"Push-upper `{new_player}` enrolled",
    )
    await bot_call_function_with_keyboard(
        bot_function=context.bot.edit_message_text,
        **tracker.bot_repr(),
    )


async def command_leave(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Leave the game.

    :param update: The event that caused the bot petition.
    :param context: The information related to the update.

    """
    try:
        tracker = get_tracker(context.chat_data)
    except NoTrackerFoundError as error:
        await bot_call_function(bot_function=update.message.reply_text, text=str(error))
        return

    player = PushUpper(update.message.from_user.id, update.message.from_user.full_name)
    try:
        tracker.leave(player)
    except PushUpperNotEnrolledError as error:
        await bot_call_function(bot_function=update.message.reply_text, text=str(error))
        return

    await update.message.delete()
    await bot_call_function(
        bot_function=context.bot.send_message,
        chat_id=tracker.chat_id,
        text=f"Push-upper `{player}` left the tracker",
    )
    await bot_call_function_with_keyboard(
        bot_function=context.bot.edit_message_text,
        **tracker.bot_repr(),
    )


async def command_reset(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Reset the game.

    :param update: The event that caused the bot petition.
    :param context: The information related to the update.

    """
    try:
        tracker = get_tracker(context.chat_data)
    except NoTrackerFoundError as error:
        await bot_call_function(bot_function=update.message.reply_text, text=str(error))
        return

    tracker.reset_push_ups()

    await bot_call_function_with_keyboard(
        bot_function=context.bot.edit_message_text,
        **tracker.bot_repr(),
    )


async def button_done(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Manage the player actions of doing the push-ups.

    :param update: The event that caused the bot petition.
    :param context: The information related to the update.

    """
    query = update.callback_query

    try:
        tracker = get_tracker(context.chat_data)
        tracker.validate_message_id(query.message.id)
    except (NoTrackerFoundError, InvalidTrackerButtonError) as error:
        await bot_call_function(bot_function=query.message.reply_text, text=str(error))
        return

    push_upper = PushUpper(query.from_user.id, query.from_user.full_name)

    answer = "You did it!"
    try:
        result_message = tracker.do(query.data, push_upper)
    except PushUpperNotEnrolledError:
        answer = "You are not enrolled in the tracker. Send /join."
        return
    except (PushUpperAlreadyDoneError, PushUpperAlreadyPunishedError) as error:
        answer = "You have done the work. Don't do it again."
        return
    finally:
        await query.answer(answer)

    await bot_call_function(
        bot_function=query.message.reply_text,
        text=result_message,
    )
    await bot_call_function_with_keyboard(
        bot_function=context.bot.edit_message_text, **tracker.bot_repr()
    )


async def distribute_punishments(context: CallbackContext) -> None:
    """Distribute the punishments when the push-up day ends.

    :param context: The information related to the update.

    """
    try:
        tracker = get_tracker(context.chat_data)
    except NoTrackerFoundError as error:
        await bot_call_function(
            bot_function=context.bot.send_message,
            chat_id=context.job.data["chat_id"],
            text=str(error),
        )
        return

    perpetrators = tracker.punish_perpetrators()

    tracker.reset_push_ups()

    await bot_call_function_with_keyboard(
        bot_function=context.bot.edit_message_text, **tracker.bot_repr()
    )

    if not perpetrators:
        await bot_call_function(
            bot_function=context.bot.send_message,
            chat_id=context.job.data["chat_id"],
            text=f"No punishments today. *Good* 👌.",
        )
        return

    perpetrators_str = "".join(f"\n· `{perpetrator.name}`" for perpetrator in perpetrators)
    await bot_call_function(
        bot_function=context.bot.send_message,
        chat_id=context.job.data["chat_id"],
        text=(
            f"The following people has to be punished:\n{perpetrators_str}\n\nPunishment:"
            f" _{random.choice(PUNISHMENTS)}_"
        ),
    )


async def remind_push_ups(context: CallbackContext) -> None:
    """Remind the players that they should do the push-ups.

    :param context: The information related to the update.

    """
    try:
        tracker = get_tracker(context.chat_data)
    except NoTrackerFoundError as error:
        await bot_call_function(
            bot_function=context.bot.send_message,
            chat_id=context.job.data["chat_id"],
            text=str(error),
        )
        return

    perpetrators = tracker.whos_guilty()

    if not perpetrators:
        return

    perpetrators_str = ", ".join(perpetrator.name for perpetrator in perpetrators)
    await bot_call_function(
        bot_function=context.bot.send_message,
        chat_id=context.job.data["chat_id"],
        text=f"Pssss... {perpetrators_str} {context.job.data['message']}",
    )


def main() -> None:
    """Start the bot."""
    load_dotenv()
    persistence = PicklePersistence(filepath="persistencefile")
    application = (
        Application.builder().token(os.getenv("BOT_TOKEN")).persistence(persistence).build()
    )
    application.add_handler(CommandHandler("help", command_help))
    application.add_handler(CommandHandler(["s", "start"], command_start))
    application.add_handler(CommandHandler(["e", "join", "enroll"], command_enroll))
    application.add_handler(CommandHandler(["l", "leave"], command_leave))
    application.add_handler(CommandHandler("reset", command_reset))
    application.add_handler(CallbackQueryHandler(button_done))

    application.run_polling()


if __name__ == "__main__":
    main()
